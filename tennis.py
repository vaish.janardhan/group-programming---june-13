#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 14:13:50 2019

@author: vaishnavi
"""

def get_score(curr_score, winner):
    curr_score[winner] += 1
    return curr_score

def init_game():
    scores = {'A':0, 'B':0}
    return scores

def get_winner(score, MIN_SCORE):
    if max(score.values()) >= MIN_SCORE:
        if score['A'] - score['B'] >= 2:
            return 'A'
        else:
            return 'B'
    return None   

def process_str(str):
    SET = 6
    GAME = 4
    sets = {'A':0,'B':0}
    games = {'A':0,'B':0}
    curr_score = init_game()
    for game in str:
        curr_score = get_score(curr_score, game)
        game_winner = get_winner(curr_score,GAME)
        if game_winner:
            games[game_winner] += 1
            curr_score = init_game()
            set_winner = get_winner(games,SET)
            if set_winner:
                sets[set_winner] += 1
    return sets,games,curr_score

input_str = 'ABAAABAABAAABAAABAABAAABAAABAABA'
sets,games,curr_score = process_str(input_str)
print(sets,games,curr_score)